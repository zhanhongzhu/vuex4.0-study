export const test = {
    namespaced: true,
    state: {
        name: '叫我詹躲躲',
        gender: '男',
        profession: '前端开发',
        age: 10
    },
    //从state派生的一些状态，可以将该部分抽离出来成函数方便调用
    getters: {
        getUserInfo: state => {
            return state.name + '的职业是' + state.profession
        },
        getUserSex: state => {
            return state.name + '的性别是' + state.gender
        }
    },
    // 更改Vuex的store中的状态的唯一方法是提交mutation，
    //每个mutation都有一个字符串的事件类型和一个回调函数，
    //该回调函数接收state作为第一个参数，提交的载荷作为第二个参数
    // 以相应的type调用store.commit方法来触发相应的回调函数
    // Mutation必须是同步函数
    mutations: {
        testMutation1(state) {
            // 变更状态
            state.age++;
        },
        // 第二个参数是载荷
        testMutation2(state, payload) {
            state.age += payload.content;
        }
    },
    // Action提交的是mutation，而不是直接变更状态
    // Action可以包含任意异步操作
    // Action函数接受一个与store实例具有相同方法和属性的context对象，因此可以调用context.commit提交一个mutation，或者通过context.state和context.getters来获取state和getters。
    // Action通过store.dispatch方法触发
    actions: {
        testAction1(context) {
            context.commit('testMutation1');
        },
        //通过参数解构来简化代码，testAction1简化为testAction2写法
        testAction2({ commit }, payload) {
            commit({
                type: 'testMutation2',
                content: payload.content
            });
        }
    }
}