import { createStore } from "vuex";
import { test } from "./modules/test";
import { test1 } from "./modules/test1";

export const store = createStore({
  // Vuex允许store分割成小的module,每个模块拥有自己的state、mutation、action、getter;
  // 访问test的状态：store.state.test
  modules: {
    test,
    test1
  }
});
